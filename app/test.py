import tensorflow as tf
from inferenceutils import load_image_into_numpy_array, run_inference_for_single_image
import math


threshold = 0.75

model = tf.saved_model.load('./saved_model')
image_np = load_image_into_numpy_array('./uploads/ktp.jpeg')
print(image_np.shape)
cols, rows, channel = image_np.shape
prediction = run_inference_for_single_image(model, image_np)


for index, score in enumerate(prediction['detection_scores']):

    if score > threshold:
        boxes = prediction['detection_boxes'][index]

        minY = math.floor(boxes[0] * cols)
        minX = math.floor(boxes[1] * rows)
        maxY = math.floor(boxes[2] * cols)
        maxX = math.floor(boxes[3] * rows)

        crop = image_np[minY:maxY, minX:maxY]
        tf.keras.utils.save_img("crop_ktp.jpeg", crop)



