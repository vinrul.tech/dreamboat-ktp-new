const fileKTP = document.getElementById("file_ktp");
const fileFoto = document.getElementById("file_foto");
const btnUpload = document.getElementById("upload");
const imgKtp = document.getElementById("img_ktp");
const imgWajahKtp = document.getElementById("img_wajah_ktp");
const imgWajahFoto = document.getElementById("img_wajah_foto");
const statusMessage = document.getElementById("status_message");
const progress = document.getElementById("progress");

let srcKtp;
let srcFoto;

let isUpload = false;

fileKTP.addEventListener("change", function () {
    console.log("FILE KTP")
    srcKtp = this.files[0];

    if (!srcKtp) {
        return;
    }
});

fileFoto.addEventListener("change", function () {
    //console.log("FILE FOTO")

    //console.log("FILE KTP")
    srcFoto = this.files[0];

    if (!srcFoto) {
        return;
    }
});



btnUpload.addEventListener("click", function (event) {

    if (isUpload) {
        return;
    }


    if (srcFoto && srcKtp) {
        resetImage();
        isUpload = true
        //btnUpload.setAttribute("disabled" , true)
        progress.style.display = "block";
        const data = new FormData();
        data.append("ktp", srcKtp);
        data.append("foto", srcFoto);

        axios.post('http://localhost:5000/upload', data)
            .then(response => {
                //console.log(response.data)
                //btnUpload.setAttribute("disabled" , false)
                isUpload = false
                progress.style.display = "none";

                const {
                    ktp,
                    wajah_ktp,
                    wajah_foto,
                    status_message
                } = response.data

                imgKtp.setAttribute("src", "/ktps/" + ktp)
                imgWajahKtp.setAttribute("src", "/fotos/" + wajah_ktp)
                imgWajahFoto.setAttribute("src", "/fotos/" + wajah_foto)
                imgKtp.style.display = "block";
                imgWajahKtp.style.display = "block";
                imgWajahFoto.style.display = "block";
                statusMessage.innerHTML = status_message

                //event.preventDefault()
            })
            .catch(error => {
                isUpload = false
                //btnUpload.setAttribute("disabled" , false)
                progress.style.display = "none";
                console.log(error.response.data.message)
                M.toast({
                    html: error.response.data.message
                });
            })
    }
});

function resetImage() {
    imgKtp.setAttribute("src", "")
    imgWajahKtp.setAttribute("src", "")
    imgWajahFoto.setAttribute("src", "")
    imgKtp.style.display = "none";
    imgWajahKtp.style.display = "none";
    imgWajahFoto.style.display = "none";
    statusMessage.innerHTML = "";
}