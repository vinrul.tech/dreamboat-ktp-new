from flask import Flask, render_template, request, redirect, jsonify, send_from_directory
from keras.backend import backend
from werkzeug.utils import secure_filename
from flask_cors import cross_origin
import os
import time
import tensorflow as tf
from inferenceutils import load_image_into_numpy_array, run_inference_for_single_image
import math
from deepface import DeepFace
import torch
import cv2
import shutil
from PIL import Image

threshold = 0.75

UPLOAD_FOLDER = './uploads'

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



app = Flask(__name__,
        static_folder='./assets',
        static_url_path=''
    )

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def init():
    global model
    #model = tf.saved_model.load('./saved_model')
    model = torch.hub.load('./yolov5', 'custom', path='./best.pt', source='local')
    model.to('cpu')


def check_ktp(model, filename):
    image_np = load_image_into_numpy_array(filename)
    cols, rows, channel = image_np.shape
    prediction = run_inference_for_single_image(model, image_np)
    for index, score in enumerate(prediction['detection_scores']):
        if score > threshold:
            boxes = prediction['detection_boxes'][index]

            minY = math.floor(boxes[0] * cols)
            minX = math.floor(boxes[1] * rows)
            maxY = math.floor(boxes[2] * cols)
            maxX = math.floor(boxes[3] * rows)

            crop = image_np[minY:maxY, minX:maxY]
            return True, crop
    
    return False, None

def check_ktp_yolov5(model, filename):
    img = cv2.imread(filename)[:, :, ::-1]
    results = model(img, size=640)
    preds = results.xyxy[0].tolist();
    
    if len(preds) > 0:
        x1 = math.floor(preds[0][0])
        y1 = math.floor(preds[0][1])
        x2 = math.floor(preds[0][2])
        y2 = math.floor(preds[0][3])

        crop = img[y1:y2, x1:x2]

        return True, crop
    
    return False, None



@app.route('/')
def index():
    return render_template('index.html')

@app.route('/ktps/<path:filename>')
def base_ktp(filename):
    return send_from_directory( app.root_path + '/uploads/ktps/', filename)

@app.route('/fotos/<path:filename>')
def base_foto(filename):
    return send_from_directory( app.root_path +  '/uploads/fotos/' , filename)

@app.route('/upload', methods=['POST'])
@cross_origin(origins="*")
def upload():
    success = False;
    message = ''
    status_message = 'not same'
    status = False
    resp_ktp =''
    resp_wajah_ktp =''
    resp_wajah_foto =''

    if request.method == 'POST':
        if 'foto' not in request.files or 'ktp' not in request.files:
            success = False
            message = 'No file part in the request'
        
        foto = request.files['foto']
        ktp = request.files['ktp']

        if foto.filename == '' or ktp.filename == '':
            success = False
            message = 'No file selected'

        if (foto and allowed_file(foto.filename))  or (ktp and allowed_file(ktp.filename)):
            file_foto = secure_filename(foto.filename)
            file_ktp = secure_filename(ktp.filename)
            timestr = time.strftime("%Y%m%d-%H%M%S")
            folder_upload = app.config['UPLOAD_FOLDER']
            folder_not_detecteds = f'{folder_upload}/not_detecteds'
            folder_ktps = f'{folder_upload}/ktps'
            folder_fotos = f'{folder_upload}/fotos'
            filename_foto = f'{timestr}-{file_foto}'
            filename_ktp = f'{timestr}-{file_ktp}'
            filename_ktp_crop = f'crop_{timestr}-{file_ktp}'
            filename_ktp_face = f'face_ktp_{timestr}-{file_ktp}'
            filename_foto_face = f'face_foto_{timestr}-{file_foto}'
            
            foto.save(os.path.join(folder_upload, filename_foto))
            ktp.save(os.path.join(folder_upload, filename_ktp))
            
            isKtp, crop = check_ktp_yolov5(model, f'{folder_upload}/{filename_ktp}')

            if isKtp :
                cv2.imwrite(f'{folder_ktps}/{filename_ktp_crop}', crop[:, :, ::-1])

                try:
                    detected_face_ktp = DeepFace.detectFace(img_path=f'{folder_ktps}/{filename_ktp_crop}', detector_backend='mtcnn')
                    tf.keras.utils.save_img(f"{folder_fotos}/{filename_ktp_face}", detected_face_ktp)
                    success = True
                except:
                    success = False
                    message = 'Face not detected'

                try:
                    detected_face = DeepFace.detectFace(img_path=f"{folder_upload}/{filename_foto}", detector_backend='mtcnn')
                    tf.keras.utils.save_img(f"{folder_fotos}/{filename_foto_face}", detected_face)
                    success = True
                except:
                    success = False
                    message = 'Face not detected'
                
                if success != False:
                    try:
                        obj = DeepFace.verify(img1_path = f'{folder_ktps}/{filename_ktp_crop}', 
                                                    img2_path = f"{folder_upload}/{filename_foto}", 
                                                    detector_backend = 'mtcnn', model_name='Facenet')
                        print(obj['distance'])
                        if obj['distance'] <= 0.5:
                            success = True
                            status = True
                            status_message = 'same'
                        elif obj['distance'] <= 0.65 and obj['distance'] > 0.5:
                            success = True
                            status = True
                            status_message = 'close same'
                        else:
                            success = True
                        
                    except:
                        success = False
                        message = 'Face not detected'
                
                
                if success != False:
                    success = True
                    message = 'Upload successful'
                    resp_ktp = filename_ktp_crop
                    resp_wajah_ktp = filename_ktp_face
                    resp_wajah_foto = filename_foto_face
                    
            else:
                shutil.move(f'{folder_upload}/{filename_ktp}', f'{folder_not_detecteds}/{filename_ktp}')
                success = False
                message = 'Identify not detected'
        else:
            success = False
            message = 'Unknown error'
        
    else:
        success = False
        message = 'Error mthod not POST'


    resp = jsonify({
        'message': message,
        'status' : status,
        'status_message' : status_message,
        'ktp' : resp_ktp,
        'wajah_ktp' : resp_wajah_ktp,
        'wajah_foto' : resp_wajah_foto,
    })
    
    if success == False:
        resp.status_code = 500
    else:
        resp.status_code = 200;

    return resp

@app.before_first_request
def execute_this():
    init()


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')